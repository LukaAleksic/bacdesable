package net.tncy.bacdesable;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Calendar;
import java.util.Date;

public class Person {

    @NotNull
    @Size(min=1)
    private String firstName;
    @NotNull
    @Size(min=1)
    private String lastName;
    @NotNull
    private Date birthDate;
    private String cityzenship;
    @NotNull
    @Min(value=18)
    private transient Integer age;

    public Person(String firstname, String lastname, Date birthdate, String cityzenship, Integer age){
        this.firstName = firstname;
        this.lastName = lastname;
        this.birthDate = birthdate;
        this.cityzenship = cityzenship;
        this.age = age;
    }

    public void computeAge(){
        if( birthDate !=null){
            Calendar calendar = Calendar.getInstance();

        }
    }
}
