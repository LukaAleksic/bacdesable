package net.tncy.bacdesable;

import org.junit.BeforeClass;
//import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
//import javax.xml.validation.Validator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class testPerson {
    public static Validator validator;

    @BeforeAll
    public static void setUpClass(){
        ValidatorFactory factory =Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }



    @Test
    public void validateAdult(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2000,Calendar.DECEMBER, 23);
        Date birthdate = calendar.getTime();

        Person person1 = new Person("Jean", "Gens", birthdate,"yes",19);
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(person1);

        assertEquals(0, constraintViolations.size());


    }

    @Test
    public void validateChild(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2010,Calendar.DECEMBER, 23);
        Date birthdate = calendar.getTime();

        Person person1 = new Person("Jean", "Gens", birthdate,"yes",9);
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(person1);

        assertEquals(1, constraintViolations.size());

    }

}
